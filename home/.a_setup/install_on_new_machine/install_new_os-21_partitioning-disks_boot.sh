#!/usr/bin/env bash

## ## ==========================================================================
## ## what to do when installing a new pc ======================================
## ## ==========================================================================

## ## PART OF SCRIPT:
## ##   after setting up partitioning
## ##   before encrypting /

## ## --------------------------------------------------------------------------
## ## create partitions on /boot -----------------------------------------------
## ## --------------------------------------------------------------------------

## ## create partition table for /boot
## command for UEFI
echo "GPT partition table is created on ${device_for_boot} (will be used for /boot and EFI partition)"
command ${enable_sudo} parted -s ${device_for_boot}                --  mklabel gpt
echo ""

## alternative for legacy BIOS
# echo "MBR partition table is created on ${device_for_boot} (will be used for /boot)"
# command ${enable_sudo} parted -s ${device_for_boot} mklabel msdos
# echo ""



## ## create boot partition non-interactively using parted
## command for UEFI
echo "create first partition (used for /boot and EFI partition)"
command ${enable_sudo} parted ${device_for_boot} --align optimal   --  mkpart "'EFI_partition'" fat32 16MiB 900MiB
command ${enable_sudo} parted ${device_for_boot}                   --  set 1 esp on
command ${enable_sudo} parted ${device_for_boot} --align optimal   --  mkpart "'stuff'" ext2 950MiB 100%
echo ""

## alternative for legacy BIOS
# command ${enable_sudo} parted ${device_for_boot} --align optimal   --  mkpart primary ext2 16MiB 900MiB
# command ${enable_sudo} parted ${device_for_boot}                   --  set 1 boot on
# command ${enable_sudo} parted ${device_for_boot} --align optimal   --  mkpart primary ext2 950MiB -1s
# echo ""

echo "device used for /boot prepared for formatting"
command ${enable_sudo} parted ${device_for_boot} print
echo ""


## check if newly created partitions are on device to be formatted
declare array_of_partitions_on_boot_device=( $(lsblk -lp -o name,type $device_for_boot | grep "part$" | cut -f1 -d\  ) )
if [[ ${array_of_partitions_on_boot_device} =~ ${device_for_boot} ]]; then
  echo ""
  echo "partition is on ${device_for_boot}"
  echo ""
else
  echo ""
  echo "CAVE: partition is NOT on ${device_for_boot} - aborting!"#; exit 1
  echo ""
fi

## ## format the partitions
echo "CAVE: the following partitions will be formated:"
echo "${array_of_partitions_on_boot_device[0]} using fat32"
echo "${array_of_partitions_on_boot_device[1]} using ext2"
sleep 5
command ${enable_sudo} mkfs.fat -F32 "${array_of_partitions_on_boot_device[0]}"
command ${enable_sudo} mkfs.ext2     "${array_of_partitions_on_boot_device[1]}"
echo ""

