#!/usr/bin/env bash

## ## ==========================================================================
## ## what to do when installing a new pc ======================================
## ## ==========================================================================

## ## PART OF SCRIPT:
## ##   after encrypting /
## ##   before preparing the installation

## ## --------------------------------------------------------------------------
## ## creating fs on / ---------------------------------------------------------
## ## --------------------------------------------------------------------------

echo "use btrfs on /"
command ${enable_sudo} mkfs.btrfs /dev/mapper/my_crypt_device
echo "mount newly created fs"
command mount /dev/mapper/my_crypt_device /mnt

echo "enable quota limits for subvolumes"
command ${enable_sudo} btrfs quote enable /mnt

echo "create btrfs subvolumes for all the needed locations"
echo "additionally, some of the subvolumes have a quota limit"
command ${enable_sudo} btrfs su cr /mnt/@root
command ${enable_sudo} btrfs qgroup limit 50G /mnt/@root
command ${enable_sudo} btrfs su cr /mnt/@var
command ${enable_sudo} btrfs qgroup limit 20G /mnt/@var
command ${enable_sudo} btrfs su cr /mnt/@tmp
command ${enable_sudo} btrfs qgroup limit 10G /mnt/@temp
command ${enable_sudo} btrfs su cr /mnt/@.snapshots
command ${enable_sudo} btrfs su cr /mnt/@swap
command ${enable_sudo} btrfs su cr /mnt/@home

echo "unmount newly created fs"
command ${enable_sudo} umount /mnt

echo "mount subvolumes using the needed options"
command ${enable_sudo} mount -o relatime,space_cache,compress=lzo,discard=async,subvol=@root           /dev/sda2   /mnt            # change '/dev/sda2' with '/dev/mapper/my_crypt_device' if needed
# relatime:     only update access time in some cases (improves performance, safe with neomutt)
# space_cache:  system knows, where free space is
# compress: zlib (slow, but high compression)
#           zstd (intermediate)
#           lzo  (fast, but low compression)
# discard:      async (free unused blocks not immediately, but in batch (reduces latency) 
#                   only use if the SSD support that - check with `lsblk --discard`, [non-zero values of the device in DISC-GRAN and DISC-MAX indicate TRIM support](https://wiki.archlinux.org/title/Solid_state_drive#TRIM))
# subvol:       what subvolume to mount
command mkdir /mnt/{boot,home,var,tmp,.snapshots}
command ${enable_sudo} mount -o noatime,space_cache,compress=zstd,discard=async,subvol=@home        /dev/mapper/my_crypt_device                 /mnt/home
command ${enable_sudo} mount -o noatime,space_cache,compress=zstd,discard=async,subvol=@tmp         /dev/mapper/my_crypt_device                 /mnt/tmp
command ${enable_sudo} mount -o noatime,space_cache,compress=zstd,discard=async,subvol=@.snapshots  /dev/mapper/my_crypt_device                 /mnt/.snapshots
command ${enable_sudo} mount -o nodatacow,subvol=@var                                               /dev/mapper/my_crypt_device                 /mnt/var
command ${enable_sudo} mount                                                                        ${array_of_partitions_on_boot_device[0]}    /mnt/boot

echo "mount swap-file"
command mkdir /mnt/swap
command ${enable_sudo} mount -o nodatacow,subvol=@swap                                              /dev/mapper/my_crypt_device                 /mnt/swap


