#!/usr/bin/env bash

## ## ==========================================================================
## ## what to do when installing a new pc ======================================
## ## ==========================================================================


## ## enable bash stict mode
set -euo pipefail
trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR
IFS=$'\n\t'
# set -x  # disable after debugging

echo "CAVE: some commands need to be run as su - do you want to use sudo?"
echo "not needed, if this script is already run as su"
enable_sudo="sudo"
#enable_sudo=" "

## ## --------------------------------------------------------------------------
## ## test for missing dependencies --------------------------------------------
## ## --------------------------------------------------------------------------
command -v dd         >/dev/null 2>&1 || { echo >&2 "coreutils    is required but not installed.  Aborting."; exit 1; }
command -v lsblk      >/dev/null 2>&1 || { echo >&2 "util-linux   is required but not installed.  Aborting."; exit 1; }
command -v efibootmgr >/dev/null 2>&1 || { echo >&2 "efibootmgr   is required but not installed.  Aborting."; exit 1; }
command -v parted     >/dev/null 2>&1 || { echo >&2 "parted       is required but not installed.  Aborting."; exit 1; }
command -v cryptsetup >/dev/null 2>&1 || { echo >&2 "cryptsetup   is required but not installed.  Aborting."; exit 1; }
command -v xkcdpass   >/dev/null 2>&1 || { echo >&2 "xkcdpass     is required but not installed.  Aborting."; exit 1; }


DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi


## ## partitioning layout
##  - separate disk (SD-card, unencrypted, at least 1GB) for /boot
##    - the EFI-disk partition will also be present here
##  - separate disk (USB-stick) for keyfile
##  - separate disk (encrypted, at least 30GB, better > 100GB) for everything
##    - full disk encryption (LUKS with detached header, see https://wiki.archlinux.org/title/Dm-crypt/Specialties#Encrypted_system_using_a_detached_LUKS_header)
##      - either LVM or btrfs with subvolumes and quotas
##        - /:          20GB up to 50GB
##        - /var:        5GB up to 20GB
##        - /tmp:        2GB up to 10GB
##        - swap:       no separate partition (only, file system doesn't support swap files)
##        - /opt:       only if you plan to install lots of software not from the standard repositories
##        - /usr/local: only if other people have root access and can install non-system-software
##        - /var/xxxxx: only if a lot of space is used by a specific service (e.g. /var/mail, /var/ftp, /var/docker, ...)
##        - /srv:       only if this is used for a specific server
##        - /home:      everything else


## ## --------------------------------------------------------------------------
## ## check for UEFI or BIOS ---------------------------------------------------
## ## --------------------------------------------------------------------------
. "$DIR/install_new_os-10_preparations-test_uefi_bios.sh"

## ## --------------------------------------------------------------------------
## ## partition disks - preparations -------------------------------------------
## ## --------------------------------------------------------------------------
. "$DIR/install_new_os-20_partitioning-disks_preparation.sh"

## ## --------------------------------------------------------------------------
## ## create partitions on /boot -----------------------------------------------
## ## --------------------------------------------------------------------------
#. "$DIR/install_new_os-21_partitioning-disks_boot.sh"

## ## --------------------------------------------------------------------------
## ## prepare / for encryption -------------------------------------------------
## ## --------------------------------------------------------------------------
. "$DIR/install_new_os-30_encrypting-root.sh"

## ## --------------------------------------------------------------------------
## ## create fs on / -----------------------------------------------------------
## ## --------------------------------------------------------------------------
. "$DIR/install_new_os-40_fs-root.sh"
