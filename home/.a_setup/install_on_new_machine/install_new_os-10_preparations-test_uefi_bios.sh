#!/usr/bin/env bash

## ## ==========================================================================
## ## what to do when installing a new pc ======================================
## ## ==========================================================================

## ## PART OF SCRIPT:
## ##   after checking for dependencies to be installed
## ##   before setting up partitioning

## ## --------------------------------------------------------------------------
## ## check for UEFI or BIOS ---------------------------------------------------
## ## --------------------------------------------------------------------------

##
## it is assumed that the PC is using Unified Extensible Firmware Interface (UEFI) with a Globally Unique Identifier (GUID) Partition Table (GPT).
##
## this can be tested using the program `efibootmgr`
echo "## is UEFI supported on the current system?"
#DISABLEDFORTESTING# efibootmgr
echo ""
[ -d /sys/firmware/efi ] && echo "This sytem supports UEFI" || echo "This system doesn't support UEFI, use the BIOS approach"
## if this returns something, computer supports UEFI
## if this returns nothing, computer does not support UEFI
echo ""


