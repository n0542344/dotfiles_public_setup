#!/usr/bin/env bash

## ## ==========================================================================
## ## what to do when installing a new pc ======================================
## ## ==========================================================================

## ## PART OF SCRIPT:
## ##   after testing for UEFI or BIOS
## ##   before partitioning /boot

## ## --------------------------------------------------------------------------
## ## partition disks 1 - preparations -----------------------------------------
## ## --------------------------------------------------------------------------

## ## test if everything is present
echo "## what devices can be used on this machine?"
echo ""
echo "just as an information: those are alle the devices and partitions"
echo "available on this machine (but dont select one of them)"
command lsblk -pf -o +uuid,type
echo ""

## prepare device for /boot
echo "only choose one of the below selected devices, not partitions!"
command lsblk -pld -o +uuid,type
echo ""
echo "please select a DEVICE for /boot and the EFI-partition"
echo "CAVE: the complete device will be used for this partition"
echo "(if you don't want that, please change the script)."
echo "CAVE: this device will be formatted as FAT32"
echo ""
read -p "name of device to create /boot:    " "device_for_boot"
echo "you selected:     ${device_for_boot}"
declare array_of_devices=( $(lsblk -dpn -o name) )
if [[ ${array_of_devices[@]} =~ ${device_for_boot} ]]; then
  echo ""
  echo "device ${device_for_boot} found"
  echo ""
else
  echo ""
  echo "CAVE: device ${device_for_boot} NOT found!"; exit 1
  echo ""
fi
echo "the device ${device_for_boot} will get overwritten to create /boot"
echo "please ensure that this is the correct device"
command lsblk -pf -o +uuid,type ${device_for_boot}
echo ""
echo ""

## prepare device for /
echo "only choose one of the below selected devices, not partitions!"
command lsblk -pld -o +uuid,type
echo ""
echo "please select a DEVICE for /"
echo "CAVE: the complete device will be used for these partitions"
echo "(if you don't want that, please change the script)."
echo "CAVE: this device will be prepared using LUKS"
echo ""
read -p "name of device to create /:    " "device_for_root"
echo "you selected:     ${device_for_root}"

echo ""
if [[ ${array_of_devices[@]} =~ ${device_for_root} ]]; then
  echo ""
  echo "device ${device_for_root} found"
  echo ""
else
  echo ""
  echo "CAVE: device ${device_for_root} NOT found!"; exit 1
  echo ""
fi
echo "the device ${device_for_root} will get overwritten to create /"
echo "please ensure that this is the correct device"
command lsblk -pf -o +uuid,type ${device_for_root}
echo ""
echo ""



