#!/usr/bin/env bash

## ## ==========================================================================
## ## what to do when installing a new pc ======================================
## ## ==========================================================================

## ## PART OF SCRIPT:
## ##   after creating boot partition
## ##   before creating filesystem on root

## ## --------------------------------------------------------------------------
## ## prepare / for encryption -------------------------------------------------
## ## --------------------------------------------------------------------------

## setup encrypted device (LUKS with detached header, similar to plain but with some advantages)
## create a header-file, size is 16M
echo "creating detached header for encrypted disk - don't forget to backup!"
command dd if=/dev/zero of=LUKS_header.img bs=16M count=1
echo ""
## create an offset of 32768 sectors @ 512 byte each (resulting in 16M)
echo "formating disk"
command ${enable_sudo} cryptsetup luksFormat ${device_for_root} --offset 32768 --header LUKS_header.img

## open the container
echo "opening cryptdevice"
command ${enable_sudo} cryptsetup open --header LUKS_header.img ${device_for_root} my_crypt_device


## see https://wiki.archlinux.org/title/Dm-crypt/Specialties#Encrypted_system_using_a_detached_LUKS_header for additional information
